Feature: User gets connection invitation
 # Need to research NFC and QR code testing

    ## PCM = Personal credential manager

    # Can these tests be parametrised like this
    @idm-pcm-e1-00015 
    @idm-pcm-e1-00011 
    @idm-pcm-e1-00010 
    Scenario Outline: User can accept connection requests
        Given connection invite arrives on PCM app
        When User interacts with the <invitation> 
        # Should a 3rd option for "Block" connection exist
        Then a prompt to "Accept" or "Refuse" is shown
        When "Accept" is selected
        Then connection is established
            And connection is stored on PCM app

        Examples:
        | Invitation | 
        | QR_Code    |  
        | NFC        |
        | URL        |


    # Can these tests be parametrised like this
    @idm-pcm-e1-00015 
    @idm-pcm-e1-00011 
    @idm-pcm-e1-00010 
    Scenario Outline: User can refuse connection requests
        Given connection invite arrives on PCM app
        When User interacts with the <invitation> 
        # Should a 3rd option for "Block" connection exist
        Then a prompt to "Accept" or "Refuse" is shown
        When "Refuse" is selected
        Then connection is denied
        # Is the denied connection permanent (on a blacklist)?

        Examples:
        | Invitation | 
        | QR_Code    |  
        | NFC        |
        | URL        |

        
 