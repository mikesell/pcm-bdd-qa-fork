Feature: User manages Verifiable Credentials
    # Scenarios: Receives (accept, refuse), removes, presents, sees, view history

    ## PCM = Personal credential manager
    ## VC = verifiable credential
    ## VP = verifiable presentation
    ## cPCM = cloud personal credential manager


    Scenario: User accepts W3C VC


    Scenario: User accepts Indy VC


    Scenario: User refuse W3C VC


    Scenario: User refuse Indy VC


    Scenario: User removes Indy VC


    Scenario: User can see VC on local PCM app (offline)


    Scenario: User can see VC on cPCM 


    Scenario: